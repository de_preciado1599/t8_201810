package model.data_structures;

/**
 *  The {@code Edge} class represents a weighted edge in an 
 *  {@link EdgeWeightedGraph}. Each edge consists of two integers
 *  (naming the two vertices) and a real-value weight. The data type
 *  provides methods for accessing the two endpoints of the edge and
 *  the weight. The natural order for this data type is by
 *  ascending order of weight.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/43mst">Section 4.3</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class Edge implements Comparable<Edge> { 

	private Vertex from;
	private Vertex to;
	private Weight weight;

	/**
	 * Initializes an edge between vertices {@code v} and {@code w} of
	 * the given {@code weight}.
	 *
	 * @param  from one vertex
	 * @param  to the other vertex
	 * @param  weight the weight of this edge
	 * @throws IllegalArgumentException if either {@code v} or {@code w} 
	 *         is a negative integer
	 * @throws IllegalArgumentException if {@code weight} is {@code NaN}
	 */
	public Edge(Vertex from, Vertex to, Weight weight) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}

	public void addWeight(Weight a�adir){
		this.weight.addWeight(a�adir.getDistanciaRecorrida(), a�adir.getValorServicioTotal(), a�adir.getTiempoViajeTotal(), a�adir.getCantPeajes());
	}

	/**
	 * Returns the weight of this edge.
	 *
	 * @return the weight of this edge
	 */
	public Weight getWeight() {
		return weight;
	}

	/**
	 * Returns either endpoint of this edge.
	 *
	 * @return either endpoint of this edge
	 */
	public Vertex either() {
		return from;
	}

	/**
	 * Returns the endpoint of this edge that is different from the given vertex.
	 *
	 * @param  vertex one endpoint of this edge
	 * @return the other endpoint of this edge
	 * @throws IllegalArgumentException if the vertex is not one of the
	 *         endpoints of this edge
	 */
	public Vertex other(Vertex vertex) {
		if      (vertex == from) return to;
		else if (vertex == to) return from;
		else throw new IllegalArgumentException("Illegal endpoint");
	}

	/**
	 * si los vertices son iguales retorna 0, si son diferentes retorna -1
	 */
	@Override
	public int compareTo(Edge that) {
		if(this.from.compareTo(that.from)==0 && this.to.compareTo(that.to)==0){
			return 0;
		}
		else
			return -1;
	}

	/**
	 * Returns a string representation of this edge.
	 *
	 * @return a string representation of this edge
	 */
	public String toString() {
		return String.format("%d-%d %.5f", from, to, weight);
	}

}
