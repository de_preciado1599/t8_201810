package Estructuras;

/**
 * 
 * @author Sebastian Mujica
 *
 */
public class Vertex implements Comparable<Vertex>{

	private Bag<Edge> adj;
	private double lat, lon;
	String idVertice ;
	LinkedList<String> serviciosProcesados;


	public Vertex(double lat, double lon) {
		this.lat = lat;
		this.lon = lon;
		idVertice=lat+","+lon;
		adj = new Bag<>();
		serviciosProcesados = new LinkedList<>();
	}

	public void addServicio(String pServicio){
		serviciosProcesados.add(pServicio);
	}


	public boolean addEdge(Edge arco){
		boolean existeArco= false;
		for (Edge edge : adj) {
			if(edge.compareTo(arco)==0){
				edge.addWeight(arco.getWeight());
				existeArco=true;
			}
		}
		if(existeArco==false)
			adj.add(arco);
		return existeArco;
	}


	/**
	 * @return the lat
	 */
	public double getLat() {
		return lat;
	}

	/**
	 * @return the lon
	 */
	public double getLon() {
		return lon;
	}

	public String getIdVertice(){
		return this.idVertice;
	}

	public Bag<Edge> getBag(){
		return this.adj;
	}

	/**
	 * si los vertices son iguales retorna 0, si son diferentes retorna -1
	 */
	@Override
	public int compareTo(Vertex o) {
		return this.idVertice.compareToIgnoreCase(o.getIdVertice());
	}

}
