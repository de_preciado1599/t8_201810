package Estructuras;

import model.vo.ServicioResumen;
/**
 * 
 * @author Sebastian Mujica
 *
 */
public class Digraph{

	private int V;
	private int E;
	private Vertex[] vertices;
	private double radioVertice;

	/**
	 * 
	 * @param V
	 * @param radioVertice
	 */
	public Digraph(int tamanio, double radioVertice) {
		if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
		this.V = 0;
		this.E = 0;
		vertices = new Vertex[tamanio];
		this.radioVertice = radioVertice;
	}

	/**
	 * 
	 * @return
	 */
	public int V(){ return this.V;}

	/**
	 * 
	 * @return
	 */
	public int E(){ return this.E;}


	/**
	 * 
	 * @param s
	 */
	public void addVertex( ServicioResumen s){
		Weight peso = new Weight(s.getTrip_miles(), s.getTrip_total(), s.getTrip_seconds(), s.getTolls());
		Vertex verInicio = new Vertex(s.getPickup_centroid_latitude(), s.getPickup_centroid_longitude());
		Vertex verFin = new Vertex(s.getDropoff_centroid_latitude(), s.getDropoff_centroid_longitude());
		Edge arco = new Edge(verInicio, verFin, peso);
		Vertex tempInicio= null;
		Vertex tempFin = null;
		//evaluar el caso en que no existe ning�n vertice, si no existe, a�ade los vertices de inicio y fin
		if(this.V==0){
			vertices[0] = verInicio;
			this.V++;
			vertices[1] = verFin;
			this.V++;
			//			addEdge(vertices[0], arco);
			vertices[0].addServicio(s.getTrip_id());
			vertices[1].addServicio(s.getTrip_id());
			tempInicio=verInicio;
			tempFin = verFin;
		}
		//eval�a el caso en el que todos los v�rtices est�n llenos

		//eval�a en qu� posici�n se deber�a agregar el nuevo servicio y sus arcos y c
		Vertex MenorDistanciaInicio= verticeMenorDistancia(s.getPickup_centroid_latitude() ,s.getPickup_centroid_longitude());
		Vertex menorDistanciaFin = verticeMenorDistancia(s.getDropoff_centroid_latitude(), s.getDropoff_centroid_longitude());

		//si los v�rtices no existen los crea y agrega los arcos
		if(MenorDistanciaInicio==null) {
			vertices[V] = verInicio;
			vertices[V].addServicio(s.getTrip_id());
			tempInicio = vertices[V];
			this.V++;
		}
		if(menorDistanciaFin==null) { 
			vertices[V] = verFin;
			vertices[V].addServicio(s.getTrip_id());
			tempFin = vertices[V];
			this.V++; 

		}
		if(MenorDistanciaInicio!=null){
			MenorDistanciaInicio.addServicio(s.getTrip_id());
			tempInicio=MenorDistanciaInicio;
		}
		if(menorDistanciaFin!=null){
			menorDistanciaFin.addServicio(s.getTrip_id());
			tempFin = menorDistanciaFin;
		}
		if(tempInicio!= null && tempFin!=null){
			if(tempInicio.compareTo(tempFin)!=0){
				if(!tempInicio.addEdge(new Edge(tempInicio, tempFin, peso)))
					this.E++;
			}
		}
	}


	/**
	 * 
	 * @param lat
	 * @param lon
	 * @return
	 */
	public Vertex verticeMenorDistancia(double lat, double lon){
		double menor = radioVertice;
		Vertex MenorDistancia = null;
		for (int i = 0; i < this.V; i++) {
			Vertex temp = vertices[i];
			if(temp!=null){
				double distancia = getDistance(lat, lon, temp.getLat(), temp.getLon());
				if(distancia<menor){
					menor = distancia;
					MenorDistancia=temp;
				}
			}
			else
				i+=vertices.length;
		}
		System.out.println(MenorDistancia);
		return MenorDistancia;
	}



	private void resize() {
		Vertex[] tmp = new Vertex[this.V*2];
		for (int i = 0; i < this.V; i++) tmp[i] = vertices[i];
		this.vertices = tmp;
	}

	void addEdge(Vertex v , Edge arco){
		v.addEdge(arco);
	}



	/**
	 * Obtener la informaci�n de un v�rtice
	 */
	//	public String getInfoVertex(K idVertex){
	//		
	//	}

	/**
	 * Modificar la informaci�n del v�rtice idVertex
	 */
	//	public void setInfoVertex(K idVertex, V infoVertex){
	//		
	//	}

	/**
	 * Obtener la informaci�n de un arco
	 * @param idVertexIni
	 * @param idVertexFin
	 * @return
	 */
	//	public String getInfoArc(K idVertexIni, K idVertexFin){
	//		
	//	}


	/**
	 * Modificar la informaci�n del arco entre los v�rtices idVertexIni e idVertexFin
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param infoArc
	 */
	//	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc){
	//		
	//	}


	/**
	 * Retorna los identificadores de los v�rtices adyacentes a idVertex
	 * @param idVertex
	 * @return
	 */
	public Iterable <Edge> adj ( double lat, double lon){
		String idVertice = lat+","+lon;
		Bag<Edge> rta = null;
		for (int i = 0; i < vertices.length; i++) {
			Vertex temp = vertices[i];
			if(temp!=null){
				if(temp.getIdVertice().equals(idVertice)){
					rta =temp.getBag();
					i+=vertices.length;
				}
			}
		}
		return rta;
	}

	public Double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final double R = (6371*1000); // Radious of the earth in meters
		Double latDistance = Math.toRadians(lat2-lat1); 
		Double lonDistance = Math.toRadians(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
		* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

}
