package model.data_structures;

public class Weight {

	private double distanciaRecorridaTotal;

	private double valorServicioTotal;

	private double tiempoViajeTotal;

	private int cantPeajes;

	private double promDistanciaRecorrida;

	private double promValorServicio;

	private double promTiempoViaje;

	private int ocurrencias;

	public Weight(double distanciaRecorrida, double valorServicio, double tiempoViaje, int peajes) {
		this.cantPeajes = 0;
		this.ocurrencias = 0;
		this.distanciaRecorridaTotal = distanciaRecorrida;
		this.valorServicioTotal = valorServicio;
		this.tiempoViajeTotal = tiempoViaje;
		if(peajes>0) this.cantPeajes++;
		ocurrencias++;
	}

	/**
	 * @return the distanciaRecorrida
	 */
	public double getDistanciaRecorrida() {
		return distanciaRecorridaTotal;
	}

	public void addWeight(double distanciaRecorrida, double valorServicio, double tiempoViaje, int peajes){
		this.distanciaRecorridaTotal += distanciaRecorrida;
		this.valorServicioTotal += valorServicio;
		this.tiempoViajeTotal += tiempoViaje;
		if(peajes>0) this.cantPeajes++;
		ocurrencias++;
		actualizarPromedio();
	}

	private void actualizarPromedio(){
		promDistanciaRecorrida = (distanciaRecorridaTotal*1.60934)/ocurrencias;
		promTiempoViaje = tiempoViajeTotal / ocurrencias;
		promValorServicio = valorServicioTotal / ocurrencias;
	}

	public int getCantPeajes(){return this.cantPeajes; }

	/**
	 * @return the promDistanciaRecorrida
	 */
	public double getPromDistanciaRecorrida() {return promDistanciaRecorrida;}

	/**
	 * @return the promValorServicio
	 */
	public double getPromValorServicio() {return promValorServicio;}

	/**
	 * @return the promTiempoViaje
	 */
	public double getPromTiempoViaje() {return promTiempoViaje;}

	/**
	 * @return the distanciaRecorridaTotal
	 */
	public double getDistanciaRecorridaTotal() {
		return distanciaRecorridaTotal;
	}

	/**
	 * @param distanciaRecorridaTotal the distanciaRecorridaTotal to set
	 */
	public void setDistanciaRecorridaTotal(double distanciaRecorridaTotal) {
		this.distanciaRecorridaTotal = distanciaRecorridaTotal;
	}

	/**
	 * @return the tiempoViajeTotal
	 */
	public double getTiempoViajeTotal() {
		return tiempoViajeTotal;
	}

	/**
	 * @param tiempoViajeTotal the tiempoViajeTotal to set
	 */
	public void setTiempoViajeTotal(double tiempoViajeTotal) {
		this.tiempoViajeTotal = tiempoViajeTotal;
	}

	/**
	 * @return the valorServicioTotal
	 */
	public double getValorServicioTotal() {
		return valorServicioTotal;
	}







}
