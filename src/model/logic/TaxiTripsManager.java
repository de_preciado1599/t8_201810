package model.logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.DiGraph;
import model.vo.ServicioResumen;


public class TaxiTripsManager implements ITaxiTripsManager {

	//__________________________________________________________________________
	// constantes
	//__________________________________________________________________________
	
	/**
	 * 
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	
	/**
	 * 
	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	
	/**
	 * 
	 */
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	
	/**
	 * 
	 */
	public static final String ARCHIVOA_100 = "./data/reporteA_100.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOA_70 = "./data/reporteA_70.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOA_25 = "./data/reporteA_25.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOB_100 = "./data/reporteB_100.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOB_70 = "./data/reporteB_70.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOB_25 = "./data/reporteB_25.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOC_100 = "./data/reporteC_100.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOC_70 = "./data/reporteC_70.txt";
	
	/**
	 * 
	 */
	public static final String ARCHIVOC_25 = "./data/reporteC_25.txt";

	//__________________________________________________________________________________
	// ATRIBUTOS
	//__________________________________________________________________________________
	
	/**
	 * 
	 */
	private DiGraph diGrafo;

	//__________________________________________________________________________________
	//carga
	//___________________________________________________________________________________
	public boolean cargarSistema(String direccionJson) 
	{
		boolean rta= false;
		diGrafo = new DiGraph(10, 25);
		if(direccionJson.equals(DIRECCION_LARGE_JSON))
		{
			int x= 2;
			while(x<9){
				String route = "./data/taxi-trips-wrvz-psew-subset-0"+x+"-02-2017.json";
				System.out.println(route);
				loadFileJson(route);
				x++;
			}			
		}
		else
		{
			
			loadFileJson(direccionJson);
			rta= true;
		}
		return rta;
	}



	/**
	 * @author Sebastian Mujica
	 * @param file
	 */
	public void loadFileJson(String file){
		try{
			JsonReader jsonReader = new JsonReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			Gson gson = new GsonBuilder().create();
			jsonReader.beginArray();
			int cont= 0;
			while (jsonReader.hasNext())
			{
				ServicioResumen s = gson.fromJson(jsonReader, ServicioResumen.class);
				if(getDistance(s.getPickup_centroid_latitude(), s.getPickup_centroid_longitude(), s.getDropoff_centroid_latitude(), s.getDropoff_centroid_longitude())>100);{
					diGrafo.addVertex(s);
				}
			}
			if(file.equals("./data/taxi-trips-wrvz-psew-subset-small.json"))
			{
				EscribirFichero(ARCHIVOA_25, "_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"+ '\n' +"REPORTE ARCHIVO SMALL CON DISTANCIA 25"+ '\n' +"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ "+ diGrafo.darInformacion());
			}
			else if(file.equals("./data/taxi-trips-wrvz-psew-subset-medium.json"))
			{
				EscribirFichero(ARCHIVOB_25, "_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"+ '\n' +"REPORTE ARCHIVO MEDIUM CON DISTANCIA 25"+ '\n' +"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ "+ diGrafo.darInformacion());
			}
			else
			{
				EscribirFichero(ARCHIVOC_25, "_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"+ '\n' +"REPORTE ARCHIVO LARGE CON DISTANCIA 25"+ '\n' +"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_ "+ diGrafo.darInformacion());
			}
//			System.out.println(diGrafo.V());
//			System.out.println(diGrafo.E());
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}




	public Double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final double R = (6371*1000); // Radious of the earth in meters
		Double latDistance = Math.toRadians(lat2-lat1); 
		Double lonDistance = Math.toRadians(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
		* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

	public void EscribirFichero(String pRuta, String pMensaje) throws IOException
	{
	        String ruta = pRuta;
	        File archivo = new File(ruta);
	        BufferedWriter bw = null;
	        if(archivo.exists()) 
	        {
	            bw = new BufferedWriter(new FileWriter(archivo));
	            bw.append("\r\n"); 
	            bw.write(pMensaje);
	            
	        
	           
	        } 
	        else 
	        {
	            System.out.println("la ruta o el archivo donde se intenta escribir no existe");
	        }
	        bw.close();
	    }
}
