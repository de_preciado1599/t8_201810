package api;

/**
 * API para la clase de logica principal  
 */
public interface ITaxiTripsManager 
{
	/**
	 * Dada la direccion del json que se desea cargar, se generan vo's, estructuras y datos necesarias
	 * @param direccionJson, ubicacion del json a cargar
	 * @return true si se lo logro cargar, false de lo contrario
	 */
	public boolean cargarSistema(String direccionJson);
	

}